<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

</section> <!-- Close #main-content -->

<footer class="site-footer">

	<div id="social-media-container">
		<div class="grid-x align-right">
			<div class="cell large-6">
				<?php if (have_rows('social_media_links', 'option')) { ?>
					<div id="social-media-links">
						<div class="button-group">
							<?php while (have_rows('social_media_links', 'option')): the_row(); ?>
								<?php $link = get_sub_field('link');
								if ($link) { ?>
									<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="button"><?php the_sub_field('icon'); ?> <?php echo $link['title']; ?></a>
								<?php } ?>
							<?php endwhile; ?>
						</div>
					</div>
				<?php } ?>	
			</div>
		</div>
	</div>
	<div id="footer-container">
		
		<div class="grid-container">
			<div class="grid-x align-center">
				<div class="cell large-9">
					<div class="grid-x grid-padding-x align-justify">

						<div class="cell small-12 medium-shrink">
							<?php foundationpress_footer_nav(); ?>
						</div>
					</div>

					<div class="grid-x grid-padding-x align-justify">
						<div class="cell small-12 medium-shrink">
							<p id="copyright">Copyright &copy; <?php echo date('Y'); ?> Premier Health. All rights reserved.</p>
						</div>
						<div class="cell small-12 medium-shrink">
							<a href="https://gatorworks.net" id="attribution">Site by Gatorworks</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="hide-for-large">
		<?php get_template_part('template-parts/navigation-quick-links'); ?>
	</div>
</footer>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php endif; ?>

<?php wp_footer(); ?>

<?php if (strpos(get_bloginfo('url'), "local")) { ?>
<script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.3'><\/script>".replace("HOST", location.hostname));
//]]></script>
<?php } ?>

</body>
</html>
