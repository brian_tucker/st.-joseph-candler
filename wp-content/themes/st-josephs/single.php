<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php get_template_part( 'template-parts/featured-image' ); ?>
		
		<div class="main-container">
			<div class="main-grid">
				<div class="main-content">
					<main>
						<?php if (has_post_thumbnail()) { ?>
							<div class="entry-featured-image">
								<?php the_post_thumbnail(); ?>
								<?php foundationpress_entry_meta(); ?>
							</div>
						<?php } else { ?>
							<div class="entry-meta">
								<?php foundationpress_entry_meta(); ?>
							</div>
						<?php } ?>
						<div class="entry-content">
							<?php the_content(); ?>
						</div>
					</main>
				
					<footer>
						<?php get_template_part('template-parts/sharing-widget'); ?>
					</footer>
				</div>
			</div>
		</div>
	</article>

	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell">
				<p class="text-center"><a href="<?php echo get_permalink(get_option('page_for_posts', true)); ?>" class="secondary"><i class="far fa-arrow-left"></i> Back to All News</a></p>
			</div>
		</div>
	</div>
<?php endwhile; ?>

<?php get_footer();
