<?php
/**
 * The template for displaying the location archive
 */

get_header(); ?>

<header class="simple-title">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell medium-11 large-10">
				<h1 class="entry-title">Clinic Locations</h1>
			</div>
		</div>
	</div>
</header>

<div class="grid-x">
	<div class="cell small-order-2 medium-order-1 large-order-1">
		<section id="clinic-finder" class="hide" data-toggler="hide">
			<div class="grid-container">
				<div class="grid-x grid-padding-x align-center">
					<div class="cell large-10">
						<?php get_template_part('template-parts/clinic-finder-form'); ?>
						<?php if (0) { ?><pre><?php var_dump($_POST); ?></pre><?php } ?>
					</div>
				</div>
			</div>
		</section>	
	</div>
	<div class="cell small-order-1 medium-order-2 large-order-2">
		<span class="show-for-sr">An embedded Google map containing our locations is included below. For a static, text-based listing of the locations, please switch to the list view by clicking the button below.</span>
		<div class="button-group large expanded" id="location-display-controls">
			<?php if ( have_posts() ) { ?>
				<a href="#" class="button" id="display-map"><i class="far fa-map-marked"></i> View Map</a>
				<a href="#" class="button midgray" id="display-list"><i class="far fa-list"></i> View List</a>
			<?php } ?>
			<a data-toggle="clinic-finder" class="button offwhite show-for-small-only"><i class="far fa-filter"></i> Filter</a>
		</div>	
	</div>
</div>

<div id="locations-container">
	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key; ?>"></script>
	<script>
		var map = null;
		var distanceMatrixCallStatus = false;
		var locationDisplay;
		function setMapActive() {
			setCookie('locationDisplay', 'map', 1);
			$('#locations-list').addClass('hide');
			$('#display-list').addClass('midgray');
			$('#locations-map').removeClass('hide');
			$('#display-map').removeClass('midgray');
		}
		function setListActive() {
			setCookie('locationDisplay', 'list', 1);
			$('#locations-map').addClass('hide');
			$('#display-map').addClass('midgray');
			$('#locations-list').removeClass('hide');
			$('#display-list').removeClass('midgray');
			<?php if (isset($_POST['latitude']) && isset($_POST['longitude']) && ($_POST['latitude'] != "") && ($_POST['longitude'] != "")) { ?>
				calcDistanceMatrices(sortLocationsByDistance);
			<?php } ?>
		}
		function sortLocationsByDistance() {
			// console.log('sort cards by distances');
			var $wrapper = $('#locations-grid');
			$wrapper.find('.location').sort(function(a, b) {
			    return +a.dataset.distance - +b.dataset.distance;
			})
			.appendTo($wrapper);
		}
		function calcDistanceMatrices() {
			var distanceMatrixCalls = 0;
			$('.location').each(function(i) {
				var lat = $(this).data('lat');
				var lng = $(this).data('lng');
				var destination = lat + ',' + lng;
				var currentLocationLat = '<?php echo $_POST['latitude']; ?>';
				var currentLocationLng = '<?php echo $_POST['longitude']; ?>';
				var origin = currentLocationLat + ',' + currentLocationLng;
				calculateDistance($(this), origin, destination);
				distanceMatrixCalls++;
				// if (distanceMatrixCalls == 3) return false;
			});
		}
		$(document).ready(function(){
			var displayPost = '<?php echo $_POST['display']; ?>';
			var displayGet = '<?php echo $_GET['display']; ?>';
			var displayCookie = getCookie('locationDisplay');
			if ((displayPost != "list") && (displayCookie != "list") && (displayGet != "list")) {
				// create map
				map = new_map( $('#locations-map') );
				<?php if (isset($_POST['latitude']) && isset($_POST['longitude']) && ($_POST['latitude'] != "") && ($_POST['longitude'] != "")) { ?>
					$(window).on('load', function() {
						map.setCenter(new google.maps.LatLng('<?php echo $_POST['latitude']; ?>', '<?php echo $_POST['longitude']; ?>'));
					});
				<?php } ?>
				setMapActive();
			} else {
				setListActive();
			}

			$('#display-map').click(function(event) {
				event.preventDefault();
				setMapActive();
				if (map == null) {
					map = new_map( $('#locations-map') );
					google.maps.event.trigger(map, 'resize');
				}
				<?php if (isset($_POST['latitude']) && isset($_POST['longitude']) && ($_POST['latitude'] != "") && ($_POST['longitude'] != "")) { ?>
					map.setCenter(new google.maps.LatLng(<?php echo $_POST['latitude']; ?>, <?php echo $_POST['longitude']; ?>));
				<?php } ?>
			});
			$('#display-list').click(function(event) {
				event.preventDefault();
				setListActive();
			});

			$(window).on('load resize orientationChange', function() {
				if (Foundation.MediaQuery.atLeast('medium')) {
					$('#clinic-finder').removeClass('hide');
				} else {
					$('#clinic-finder').addClass('hide');
				}
			});

			$(window).on('load', function() {
				$('#locations-map').css('opacity', 1);
				$('#locations-list').css('opacity', 1);
				$('#loading-animation').fadeOut();
			});
		});
	</script>

	<?php if ( !have_posts() ) : ?>
		<p class="text-center" id="locations-results-empty-message">Sorry, but nothing matched your search. Please try again or <a href="<?php echo get_post_type_archive_link( 'location' ); ?>">clear the filter and view all locations</a>.</p>
	<?php endif; // End have_posts() check. ?>
		<div id="locations-map" class="google-map" data-map-type-control="false" style="opacity: 0;">
			<?php if ( have_posts() ) : ?>
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php $location = get_field('location'); ?>
					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
						<div class="location-card info-window" data-location="<?php echo $post->post_name; ?>">
							<div class="grid-x grid-margin-x">
								<?php if (has_post_thumbnail()) { ?>
									<div class="cell shrink">
										<img style="width: 100px;" src="<?php echo get_the_post_thumbnail_url(null, 'thumbnail'); ?>" alt="<?php the_title(); ?>" />
									</div>
								<?php } ?>
								<div class="cell auto">
									<h2 class="h5 location-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<p class="address"><?php echo get_address_string($post->ID); ?></p>
									<?php if (get_field('phone') != "") { ?>
									<p><a class="phone" href="tel:<?php echo stripPhoneForLink(get_field('phone')); ?>"><?php the_field('phone'); ?></a></p>
									<?php } ?>
								</div>
							</div>
							<div><hr></div>
							<p class="hours"><?php the_field('hours'); ?></p>
							<?php 
								$clockwisemd_id = get_field('clockwisemd_id'); 
								$virtual_visit = get_field('virtual_visit_clockwisemd_id'); 
								$hidden = get_field('hide_virtual_visit');
							?>
				
							
							<div class="button-group small expanded">
								<?php if($hidden): ?>

								<?php else: ?>
									<?php if ($virtual_visit != "") { ?>
										<a href="https://www.clockwisemd.com/hospitals/<?php echo $virtual_visit; ?>/visits/new" class="button secondary" target="_blank">Virtual Visit</a>
									<?php } else { ?>
										<a href="https://www.clockwisemd.com/hospitals/<?php echo $virtual_visit; ?>/visits/new" class="button secondary" target="_blank">Virtual Visit</a>
									<?php } ?>
								<?php endif; ?>
								
								
								<a href="https://www.clockwisemd.com/hospitals/<?php echo $clockwisemd_id; ?>/visits/new" class="button" target="_blank">Check-In</a>
							</div>
								
							
							<?php $directions_url = 'https://www.google.com/maps/dir/?api=1&origin=&destination=' . urlencode(str_replace("<br>", " ", get_address_string($post->ID))); if (get_field('google_place_id') != "") { 
								$directions_url .= '&destination_place_id=' . get_field('google_place_id');
							} ?>
							<div class="button-group small expanded" style="margin-top: 0.5rem;">
								<a href="<?php echo $directions_url; ?>" class="button white expanded" target="_blank" style="background: #eee" data-location="<?php echo $post->post_name; ?>">Directions</a>
							</div>
				
							<div><hr></div>
							<div class="ride-services grid-x grid-margin-x">
								<div class="cell small-6"><p>Need a ride?</p></div>
								<div class="cell small-6">
									<?php $uber_url = 'https://m.uber.com/ul/?action=setPickup&pickup=my_location'; 
									$uber_url .= '&' . urlencode('dropoff[formatted_address]') . '=' . urlencode(str_replace("<br>", " ", get_address_string($post->ID)));
									$uber_url .= '&' . urlencode('dropoff[latitude]') . '=' . $location['lat'];
									$uber_url .= '&' . urlencode('dropoff[longitude]') . '=' . $location['lng']; ?>
									<a href="<?php echo $uber_url; ?>" class="uber" title="Get a ride with Uber" target="_blank">
										<i class="fab fa-uber"></i><span class="show-for-sr">Get a ride with Uber</span>
									</a>
									<?php $lyft_url = 'https://lyft.com/ride?id=lyft&partner=NuWwQpN9mi2J';
									// $lyft_url .= urlencode('&dropoff[formatted_address]=' . str_replace("<br>", " ", get_address_string($post->ID)));
									$lyft_url .= '&' . urlencode('pickup[latitude]') . '=' . $_POST['latitude'];
									$lyft_url .= '&' . urlencode('pickup[longitude]') . '=' . $_POST['longitude'];
									$lyft_url .= '&' . urlencode('destination[latitude]') . '=' . $location['lat'];
									$lyft_url .= '&' . urlencode('destination[longitude]') . '=' . $location['lng']; 

									$full_clinic_name = get_field('full_clinic_name');
									if ($full_clinic_name == "") $full_clinic_name = 'Lake After Hours - ' . get_the_title(); ?>
									<a href="<?php echo $lyft_url; ?>" class="lyft" title="Get a ride with Lyft" target="_blank" data-post-id="<?php echo $post->ID; ?>">
										<i class="fab fa-lyft"></i><span class="show-for-sr">Get a ride with Lyft</span>
									</a>
									<div class="reveal" id="a-reveal-<?php echo $post->ID; ?>" data-reveal>
										<p>We cannot connect to Lyft without your current location. Please proceed to the Lyft app with the link below and search for '<?php echo $full_clinic_name; ?>' as your destination.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>

			<?php if (isset($_POST['latitude']) && isset($_POST['longitude']) && ($_POST['latitude'] != "") && ($_POST['longitude'] != "")) { ?>
				<div class="marker current-location" data-lat="<?php echo $_POST['latitude']; ?>" data-lng="<?php echo $_POST['longitude']; ?>" data-icon-path="circle" data-fill-color="#205A90">
					<h6>Current Location</h6>
				</div>
			<?php } ?>
		</div>

	<div id="locations-list" class="hide" style="opacity: 0;">
		<div class="grid-container">
			<div class="grid-x grid-padding-x align-center">
				<div class="cell medium-11 large-10">
					<?php if (isset($_GET['checkin']) && ($_GET['checkin'] == "true")) { ?>
						<div class="callout warning">
							<p>Select a location below to check in, or schedule a virtual visit.</p>
						</div>
					<?php } ?>

					<?php if ( have_posts() ) : ?>
						<div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-3" id="locations-grid">
							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php $location = get_field('location'); ?>
								<div class="cell location" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
									<div class="card location-card">
										<a href="<?php the_permalink(); ?>">
											<?php if (has_post_thumbnail()) { ?>
												<img class="location-image" data-interchange="[<?php echo get_the_post_thumbnail_url(null, 'thumbnail'); ?>, small], [<?php echo get_the_post_thumbnail_url(null, 'featured-small'); ?>, medium],[<?php echo get_the_post_thumbnail_url(null, 'featured-small'); ?>, large]" alt="<?php the_title(); ?>">
											<?php } else { ?>
												<img class="location-image" data-interchange="[https://placehold.it/200x200/D5DBDF?text=<?php echo urlencode(get_the_title()); ?>, small], [https://placehold.it/600x275/D5DBDF?text=<?php echo urlencode(get_the_title()); ?>&fontsize=16, medium],[https://placehold.it/600x275/D5DBDF?text=<?php echo urlencode(get_the_title()); ?>&fontsize=16, large]" alt="<?php the_title(); ?>">
											<?php } ?>
										</a>
										<div class="card-section">
											<?php $logo = get_field('logo');
											if (!empty($logo)) { ?>
												<a href="<?php the_permalink(); ?>" class="location-logo-container show-for-medium">
													<img class="location-logo" src="<?php echo $logo['sizes']['medium']; ?>" alt="logo for <?php the_title(); ?>">
												</a>
											<?php } ?>
											<div class="location-meta">
												<?php $map_url = 'https://www.google.com/maps/search/?api=1&query=' . urlencode(str_replace("<br>", " ", get_address_string($post->ID)));
												if (get_field('google_place_id') != "") {
													$map_url .= '&query_place_id=' . get_field('google_place_id');
												} ?>
												<div class="grid-x grid-margin-x align-right">
													<div class="cell small-6 text-right">
														<a href="<?php echo $map_url; ?>" target="_blank" class="map-link secondary">View on Map</a>
													</div>
												</div>
												<div class="grid-x grid-margin-x primary-location-info">
													<div class="cell small-9 small-offset-3 medium-12 medium-offset-0">
														<?php //echo get_linear_distance($_POST['latitude'], $_POST['longitude'], $location['lat'], $location['lng']); ?>
														<p class="distance hide"><strong>Distance:</strong> <span class="distance-value"></span></p>
														<h2 class="h5 location-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
														<p class="address"><?php echo get_address_string($post->ID); ?></p>
														<?php if (get_field('phone') != "") { ?>
														<p><a class="phone" href="tel:<?php echo stripPhoneForLink(get_field('phone')); ?>"><?php the_field('phone'); ?></a></p>
														<?php } ?>
													</div>
												</div>
												<div><hr></div>
												<p class="hours"><?php the_field('hours'); ?></p>
												<?php 
													$clockwisemd_id = get_field('clockwisemd_id'); 
													$virtual_visit = get_field('virtual_visit_clockwisemd_id'); 
													$hidden = get_field('hide_virtual_visit');
												?>
											</div>
											
											
											<div class="button-group small expanded">
												<?php if($hidden): ?>

												<?php else: ?>
													<?php if ($virtual_visit != "") { ?>
														<a href="https://www.clockwisemd.com/hospitals/<?php echo $virtual_visit; ?>/visits/new" class="button secondary" target="_blank">Virtual Visit</a>
													<?php } else { ?>
														<a href="https://www.clockwisemd.com/hospitals/<?php echo $virtual_visit; ?>/visits/new" class="button secondary" target="_blank">Virtual Visit</a>
													<?php } ?>
												<?php endif; ?>
												<a href="https://www.clockwisemd.com/hospitals/<?php echo $clockwisemd_id; ?>/visits/new" class="button" target="_blank">Check-In</a>
											</div>
												
											
											<?php $directions_url = 'https://www.google.com/maps/dir/?api=1&origin=&destination=' . urlencode(str_replace("<br>", " ", get_address_string($post->ID))); if (get_field('google_place_id') != "") { 
												$directions_url .= '&destination_place_id=' . get_field('google_place_id');
											} ?>

											<div class="button-group small expanded" style="margin-top: 0.5rem;">
												<a href="<?php echo $directions_url; ?>" class="button white expanded" target="_blank" data-location="<?php echo $post->post_name; ?>">Directions</a>
											</div>
											
											<div><hr></div>
											<div class="ride-services grid-x grid-margin-x">
												<div class="cell small-6"><p>Need a ride?</p></div>
												<div class="cell small-6">
													<?php $uber_url = 'https://m.uber.com/ul/?action=setPickup&pickup=my_location'; 
													$uber_url .= '&' . urlencode('dropoff[formatted_address]') . '=' . urlencode(str_replace("<br>", " ", get_address_string($post->ID)));
													$uber_url .= '&' . urlencode('dropoff[latitude]') . '=' . $location['lat'];
													$uber_url .= '&' . urlencode('dropoff[longitude]') . '=' . $location['lng']; ?>
													<a href="<?php echo $uber_url; ?>" class="uber" title="Get a ride with Uber" target="_blank">
														<i class="fab fa-uber"></i><span class="show-for-sr">Get a ride with Uber</span>
													</a>
													<?php $lyft_url = 'https://lyft.com/ride?id=lyft&partner=NuWwQpN9mi2J';
													// $lyft_url .= urlencode('&dropoff[formatted_address]=' . str_replace("<br>", " ", get_address_string($post->ID)));
													$lyft_url .= '&' . urlencode('pickup[latitude]') . '=' . $_POST['latitude'];
													$lyft_url .= '&' . urlencode('pickup[longitude]') . '=' . $_POST['longitude'];
													$lyft_url .= '&' . urlencode('destination[latitude]') . '=' . $location['lat'];
													$lyft_url .= '&' . urlencode('destination[longitude]') . '=' . $location['lng']; 

													$full_clinic_name = get_field('full_clinic_name');
													if ($full_clinic_name == "") $full_clinic_name = 'Lake After Hours - ' . get_the_title(); ?>
													<a href="<?php echo $lyft_url; ?>" class="lyft" title="Get a ride with Lyft" target="_blank" data-post-id="<?php echo $post->ID; ?>">
														<i class="fab fa-lyft"></i><span class="show-for-sr">Get a ride with Lyft</span>
													</a>
													<div class="reveal" id="a-reveal-<?php echo $post->ID; ?>" data-reveal>
														<p>We cannot connect to Lyft without your current location. Please proceed to the Lyft app with the link below and search for '<?php echo $full_clinic_name; ?>' as your destination.</p>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						</div>

						<script>
							$(document).ready(function() {
								<?php if ((!isset($_POST['latitude'])) || ($_POST['latitude'] == '')) { ?>
									$('a.lyft').click(function(event) {
										event.preventDefault();
										var link_url = $(this).attr('href');
										var post_id = $(this).data('post-id');
										// console.log('a-reveal-'+post_id);
										$('#a-reveal-'+post_id).foundation('open');
									});
								<?php } ?>
							})
						</script>
						
						<?php if (isset($_POST['latitude']) && isset($_POST['longitude']) && ($_POST['latitude'] != "") && ($_POST['longitude'] != "")) { ?>
							<script>
								$(document).ready(function() {
									//calcDistanceMatrices();
								});
							</script>
						<?php } ?>

					<?php else : ?>
						<p>Sorry, but nothing matched your search. Please try again or <a href="<?php echo get_post_type_archive_link( 'location' ); ?>">clear the filter and view all locations</a>.</p>
					<?php endif; // End have_posts() check. ?>
				</div>
			</div>
		</div>
	</div>

	<div id="loading-animation">
		<div class="icon-container">
			<i class="fad fa-spinner-third fa-spin fa-8x"></i>
		</div>
	</div>
</div>

<?php get_footer();
