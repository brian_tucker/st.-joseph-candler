<?php
/*
Template Name: Front
*/
get_header(); ?>

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
		<?php $featured_image = array();
		array_push($featured_image, get_the_post_thumbnail_url( null, 'large' ));
		array_push($featured_image, get_the_post_thumbnail_url( null, 'large' ));
		array_push($featured_image, get_the_post_thumbnail_url( null, 'xlarge' ));
		array_push($featured_image, get_the_post_thumbnail_url( null, 'full' )); ?>

		<header id="front-hero" role="banner" data-interchange="[<?php echo $featured_image[0]; ?>, small], [<?php echo $featured_image[1]; ?>, medium], [<?php echo $featured_image[2]; ?>, large], [<?php echo $featured_image[3]; ?>, xlarge]">
			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			<div class="header-content">
				<div class="grid-container">
					<div class="grid-x grid-padding-x grid-padding-y align-center">
						<div class="cell small-10 large-6">
							<div class="content-container">
								<div class="hero-content">
									<?php the_content(); ?>
									
									<?php if (have_rows('hero_buttons')) { ?>
										<div class="button-group expanded">
											<?php while (have_rows('hero_buttons')): the_row(); ?>
												<?php $link = get_sub_field('link');
												if ($link) { ?>
													<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="button large white"><?php echo $link['title']; ?></a>
												<?php } ?>
											<?php endwhile; ?>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="#clinic-finder" id="scroll-to-content" data-smooth-scroll>Scroll For More <i class="fas fa-arrow-down"></i></a>
		</header>

		<section id="clinic-finder">
			<div class="grid-container">
				<div class="grid-x grid-padding-x align-center">
					<div class="cell large-8">
						<?php the_field('clinic_finder_content'); ?>
						<?php get_template_part('template-parts/clinic-finder-form'); ?>
					</div>
				</div>
			</div>
		</section>

		<?php $image = get_field('differentiators_background_image');
		$background_string = '';
		$background_class = '';
		if (!empty($image)) {
			$background_string = 'data-interchange="[' . $image['sizes']['medium'] . ', small], [' . $image['sizes']['large'] . ', medium], [' . $image['sizes']['xlarge'] . ', large]"';
			$background_class = 'has-background-image';
		} ?>
		<section id="differentiators" class="<?php echo $background_class; ?>" <?php echo $background_string; ?>>
			<div class="grid-container">
				<div class="grid-x grid-padding-x align-center">
					<div class="cell large-9">
						<?php the_field('differentiators_intro_content'); ?>
						<?php if (have_rows('differentiators_list')) { ?>
							<div class="grid-x grid-padding-x grid-padding-y align-center" id="differentiator-list-items">
								<?php while (have_rows('differentiators_list')): the_row(); ?>
									<div class="cell small-12 medium-4 large-auto">
										<div class="list-item">
											<div class="list-item-icon">
												<?php if (get_sub_field('icon') != "") { ?>
													<?php the_sub_field('icon'); ?>
												<?php } ?>
											</div>
											<div class="list-item-text">
												<?php the_sub_field('title'); ?>
											</div>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>

		<section id="brands">
			<div class="grid-container">
				<div class="grid-x grid-padding-x align-center">
					<div class="cell large-8" id="brands-content-container">
						<div class="grid-x grid-padding-x">
							<div class="cell large-6">
								<?php the_field('branding_intro'); ?>
							</div>
						</div>
						<div class="grid-x grid-padding-x">
							<div class="cell">
								<?php if (have_rows('branding_logos')) { ?>
									<hr>
									<div class="grid-x grid-padding-x align-center align-middle">
										<?php while (have_rows('branding_logos')): the_row(); ?>
											<?php $logo = get_sub_field('logo');
											if (!empty($logo)) { ?>
												<div class="cell small-6">
													<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
												</div>
											<?php } ?>
										<?php endwhile; ?>
									</div>
									<hr>
								<?php } ?>
								<div class="grid-x grid-padding-x align-center align-middle">
									<div class="cell small-6">
										<p class="text-center large-text-left"><?php the_field('branding_subheading') ?></p>
									</div>
									<div class="cell small-6">
										<?php $button = get_field('branding_button');
										if ($button) { ?>
											<p class="text-center large-text-right">
												<a href="<?php echo $button['url']; ?>" class="button secondary" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
											</p>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="testimonial-news">
			<div class="grid-container">
				<div class="grid-x grid-padding-x align-center">
					<div class="cell large-8">
						<div class="grid-x grid-padding-x">
							<div class="cell large-6">
								<blockquote class="wp-block-testimonial">
									<h4>Patient Stories</h4>
									<?php the_field('testimonial_content'); ?>
									<cite class="testimonial-source"><?php the_field('testimonial_citation'); ?></cite>
								</blockquote>
							</div>
							<div class="cell large-6" id="news-column">
								<h4 class="column-heading">Latest News</h4>
								<?php // WP_Query arguments
								$args = array(
									'post_type'              => array( 'post' ),
									'post_status'            => array( 'publish' ),
									'posts_per_page'         => '3',
								);

								// The Query
								$latest_news_query = new WP_Query( $args );

								// The Loop
								if ( $latest_news_query->have_posts() ) {
									while ( $latest_news_query->have_posts() ) {
										$latest_news_query->the_post(); ?>
										<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
											<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Read the article">', '</a></h2>' ); ?>
											<?php the_excerpt(); ?>
										</article>
										<hr>
									<?php }
								} else {
									// no posts found
								}

								// Restore original Post Data
								wp_reset_postdata(); ?>
								
								<p class="text-center large-text-left" id="read-more">
									<a href="<?php echo get_post_type_archive_link( 'post' ); ?>" class="button secondary">More News</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</article>
<?php endwhile; ?>
<?php do_action( 'foundationpress_after_content' ); ?>

<div id="mobile-splash-pane" class="hide-for-medium" style="position:fixed; top:0; left:0; width:100%; height:100%; z-index:100; overflow-y:scroll;">
	<div id="intro-logo" style="opacity:0;">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/watermark.svg" alt="logo watermark">
	</div>

	<div id="quick-buttons" style="opacity:0;">
		<div class="grid-y" style="height: 100%;">
			<div class="cell shrink" id="logo-section">
				<a href="#" data-action="close-splash-screen" class="logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<?php $logo = get_field('logo','options'); ?>
						<img src="<?php echo $logo['sizes']['medium_large']; ?>" alt="<?php echo $logo['alt'] ?>" class="logo">
					</a>
				</a>
			</div>

			<div class="cell auto" id="menu-section">
				<ul class="splash-pane-menu menu grid-y">
					<li class="cell">
						<a href="<?php echo get_post_type_archive_link( 'location' ); ?>?display=list&checkin=true" class="button large expanded squared secondary">
							<i class="far fa-comment-alt-check"></i>
							Check-In
						</a>
					</li>
					<?php if (have_rows('mobile_splash_pane_buttons', 'option')) { ?>
						<?php while (have_rows('mobile_splash_pane_buttons', 'option')): the_row(); ?>
							<?php $link = get_sub_field('link'); ?>
							<li class="cell">
							    <a href="<?php echo $link['url']; ?>" class="button large expanded squared">
							        <div class="button-content">
						                <?php the_sub_field('icon'); ?>
						                <span><?php echo $link['title']; ?></span>
							        </div>
							    </a>
							</li>
						<?php endwhile; ?>
					<?php } ?>
					<li class="cell">
						<a href="#" id="close-splash-screen" data-action="close-splash-screen" class="button large expanded squared">
							<i class="far fa-laptop-medical"></i>
							Visit Full Site
						</a>
					</li>
				</ul>	
			</div>
		</div>
	</div>
</div>

<script>
	var showSplashPane = true;
	function isBot(){
		var botPattern = "(googlebot\/|Googlebot-Mobile|Googlebot-Image|Google favicon|Mediapartners-Google|bingbot|slurp|java|wget|curl|Commons-HttpClient|Python-urllib|libwww|httpunit|nutch|phpcrawl|msnbot|jyxobot|FAST-WebCrawler|FAST Enterprise Crawler|biglotron|teoma|convera|seekbot|gigablast|exabot|ngbot|ia_archiver|GingerCrawler|webmon |httrack|webcrawler|grub.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|bibnum.bnf|findlink|msrbot|panscient|yacybot|AISearchBot|IOI|ips-agent|tagoobot|MJ12bot|dotbot|woriobot|yanga|buzzbot|mlbot|yandexbot|purebot|Linguee Bot|Voyager|CyberPatrol|voilabot|baiduspider|citeseerxbot|spbot|twengabot|postrank|turnitinbot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|blekkobot|ezooms|dotbot|Mail.RU_Bot|discobot|heritrix|findthatfile|europarchive.org|NerdByNature.Bot|sistrix crawler|ahrefsbot|Aboundex|domaincrawler|wbsearchbot|summify|ccbot|edisterbot|seznambot|ec2linkfinder|gslfbot|aihitbot|intelium_bot|facebookexternalhit|yeti|RetrevoPageAnalyzer|lb-spider|sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web-archive-net.com.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|seokicks-robot|it2media-domain-crawler|ip-web-crawler.com|siteexplorer.info|elisabot|proximic|changedetection|blexbot|arabot|WeSEE:Search|niki-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|Lipperhey SEO Service|CC Metadata Scaper|g00g1e.net|GrapeshotCrawler|urlappendbot|brainobot|fr-crawler|binlar|SimpleCrawler|Livelapbot|Twitterbot|cXensebot|smtbot|bnf.fr_bot|A6-Indexer|ADmantX|Facebot|Twitterbot|OrangeBot|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|SemrushBot|yoozBot|lipperhey|y!j-asr|Domain Re-Animator Bot|AddThis)";
		var re = new RegExp(botPattern, 'i');
		var userAgent = navigator.userAgent;
		if (re.test(userAgent)) {
			return true;
		} else {
			return false;
		}
	}

	$(window).on('scroll', function() {
		if ($(document).scrollTop() > 25) {
			$('#mobile-navigation').addClass('scrolled');
		} else {
			$('#mobile-navigation').removeClass('scrolled');
		}
	});

	function closeSplashPane() {
		$('#mobile-splash-pane').css('opacity', '0');
		$('body').removeClass('no-scroll');
		setTimeout(function() {
			$('#mobile-splash-pane').remove();
		}, 300);
		showSplashPane = false;
	}

	$(window).on('load', function() {
		if (isBot() || Foundation.MediaQuery.atLeast('medium') || (sessionStorage.getItem("dismissedFrontSplash")=="1")) {
			closeSplashPane();
		} else if (showSplashPane) {
			$('body').addClass('no-scroll');
			$logo = $('#intro-logo');
			Foundation.onImagesLoaded($logo, function() {
				$('#intro-logo').css('opacity', '1');
				setTimeout(function() {
					$('#intro-logo').css('opacity', '0');
				}, 2000);
				setTimeout(function() {
					$('#quick-buttons').css('opacity', '1');
				}, 2300);
			});
		}

		$('[data-action="close-splash-screen"]').click(function(event) {
			event.preventDefault();
			sessionStorage.setItem("dismissedFrontSplash", "1");
			closeSplashPane();
		});
	});
</script>

<?php get_footer();
