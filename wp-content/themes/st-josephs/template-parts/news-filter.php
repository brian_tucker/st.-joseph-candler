<?php  ?>
<div id="news-filter">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x grid-padding-y align-center">
			<div class="cell medium-11 large-10">
				<form action="">
					<label for="cat">Filter by Category
						<?php wp_dropdown_categories( 'show_option_none=All News' ); ?>
					</label>
					<script type="text/javascript">
						<!--
						var dropdown = document.getElementById("cat");
						function onCatChange() {
							if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
								location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
							} else if ( dropdown.options[dropdown.selectedIndex].value == -1 ) {
								location.href = "<?php echo get_permalink(get_option('page_for_posts', true)); ?>";
							}
						}
						dropdown.onchange = onCatChange;
						-->
					</script>
				</form>
			</div>
		</div>
	</div>
</div>