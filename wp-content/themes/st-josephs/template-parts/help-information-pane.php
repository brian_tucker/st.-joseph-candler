<?php
/**
 * Template part for off canvas menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div class="off-canvas position-right" id="help-information-pane" data-off-canvas data-auto-focus="true" data-transition="overlap" data-content-scroll="false" role="navigation">
	<div class="grid-x">
		<div class="cell small-12">
			<?php if (have_rows('help_information_faqs', 'option')) { ?>
				<ul class="accordion" data-accordion>
					<?php while (have_rows('help_information_faqs', 'option')): the_row(); ?>
					  	<li class="accordion-item" data-accordion-item>
					    	<a href="#" class="accordion-title"><?php the_sub_field('question'); ?></a>
						    <div class="accordion-content" data-tab-content>
						      	<?php the_sub_field('answer'); ?>
						    </div>
					  	</li>
					<?php endwhile; ?>
				</ul>
			<?php } ?>

			<button class="show-on-focus button expanded squared lightgray" aria-label="Close Help Information Pane" type="button" data-close><i class="far fa-times"></i> Close</button>
		</div>
	</div>
</div>
