<?php

/**
 * This is the template that renders the accordion block.
 *
 * @param   array $block The block settings and attributes.
 * @param   bool $is_preview True during AJAX preview.
 */

// create id attribute for specific styling
$id = 'click-to-expand-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>

<div class="wp-block-click-to-expand <?php echo $align_class; ?>" id="<?php echo $id; ?>">
	<?php if (get_field('heading') != "") { ?>
		<h3 class="h4 text-center block-heading"><?php the_field('heading'); ?></h3>
	<?php } ?>

	<?php if (have_rows('items')) { 
		$items = get_field('items');
		$len = count($items);
		if ($len < 6) {
			$split = $len;
		} else if ($len % 2 == 0) {
			$split = $len / 2;
		} else {
			$split = $len / 2 + 1;
		}

		$firsthalf = array_slice($items, 0, $split);
		$secondhalf = array_slice($items, $split); ?>
		
		<div class="tabs-container click-to-expand-tabs-container show-for-large" style="min-height:<?php echo count($firsthalf)*100; ?>px;">
			<ul class="tabs vertical" data-tabs id="click-to-expand-tabs">
			  	<?php $side_count = 0;
			  	$item_id = 1;
			  	foreach ($firsthalf as $item) { ?>
					<li class="tabs-title left <?php if ($item_id==1) echo 'is-active'; ?> count-<?php echo $side_count; ?>" style="top:<?php echo $side_count*100; ?>px;">
				  		<a class="tabs-link" href="#item-content-<?php echo $item_id; ?>" <?php if ($item_id==1) echo 'aria-selected="true"'; ?>>
				  			<?php if ($item['icon'] != "other") { ?>
			    				<i class="<?php echo $item['icon']; ?>"></i>
			    			<?php } else { ?>
			    				<?php echo $item['other_icon_markup']; ?>
			    			<?php } ?>
			    			<?php echo $item['title']; ?>
			    		</a>
				    </li>
				    <?php $side_count++; $item_id++; ?>
				<?php } ?>
				<?php $side_count = 0;
			  	foreach ($secondhalf as $item) { ?>
					<li class="tabs-title right count-<?php echo $side_count; ?>" style="top:<?php echo $side_count*100; ?>px;">
				  		<a class="tabs-link" href="#item-content-<?php echo $item_id; ?>">
				  			<?php if ($item['icon'] != "other") { ?>
			    				<i class="<?php echo $item['icon']; ?>"></i>
			    			<?php } else { ?>
			    				<?php echo $item['other_icon_markup']; ?>
			    			<?php } ?>
			    			<?php echo $item['title']; ?>
			    		</a>
				    </li>
				    <?php $side_count++; $item_id++; ?>
				<?php } ?>

				<?php if (($len > 5) && ($len % 2 != 0)) { ?>
					<li class="tabs-placeholder right count-<?php echo $side_count; ?>" style="top:<?php echo $side_count*100; ?>px;">
					</li>
				<?php } ?>
			</ul>
			<div class="tabs-content click-to-expand-tabs-content-container" data-tabs-content="click-to-expand-tabs">
				<?php $service_id = 1; ?>
				<?php while (have_rows('items')): the_row(); ?>
					<div id="item-content-<?php echo $service_id; ?>" class="tabs-panel <?php if ($service_id==1) echo 'is-active'; ?>" data-service="<?php echo $service_id; ?>">
						<div class="icon">
							<?php if (get_sub_field('icon') != "other") { ?>
			    				<i class="<?php the_sub_field('icon'); ?>"></i>
			    			<?php } else { ?>
			    				<?php the_sub_field('other_icon_markup'); ?>
			    			<?php } ?>
			    		</div>
						<h3 class="item-title"><?php the_sub_field('title'); ?></h3>
				  		<?php the_sub_field('content'); ?>
				    </div>
				    <?php $service_id++; ?>
			  	<?php endwhile; ?>
			</div>
		</div>
		
		<div class="grid-container hide-for-large">
			<div class="grid-x grid-padding-x">
				<div class="cell">
					<ul class="accordion click-to-expand-accordion" data-accordion data-allow-all-closed="true">
						<?php while (have_rows('items')): the_row(); ?>
							<li class="accordion-item" data-accordion-item>
						  		<a class="accordion-title" href="#">
						  			<?php if (get_sub_field('icon') != "other") { ?>
					    				<i class="<?php the_sub_field('icon'); ?>"></i>
					    			<?php } else { ?>
					    				<?php the_sub_field('other_icon_markup'); ?>
					    			<?php } ?>
					    			<?php the_sub_field('title'); ?>
					    		</a>
						  		<div class="accordion-content" data-tab-content>
						    		<?php the_sub_field('content'); ?>
						    	</div>
						    </li>
					  	<?php endwhile; ?>
					</ul>
				</div>
			</div>
		</div>
	<?php } ?>
</div>