<?php

/**
 * This is the template that renders the accordion block.
 *
 * @param   array $block The block settings and attributes.
 * @param   bool $is_preview True during AJAX preview.
 */

// create id attribute for specific styling
$id = 'accordion-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>

<div class="wp-block-accordion <?php echo $align_class; ?>" id="<?php echo $id; ?>">
	<?php if (get_field('heading') != "") { ?>
		<h3><?php the_field('heading'); ?></h3>
	<?php } ?>
	<?php if (have_rows('accordion_items')) { ?>
		<ul class="accordion" data-accordion data-allow-all-closed="true">
			<?php while (have_rows('accordion_items')): the_row(); ?>
		  		<li class="accordion-item" data-accordion-item>
		    		<a href="#" class="accordion-title"><?php the_sub_field('title'); ?></a>

		    		<div class="accordion-content" data-tab-content>
		    			<?php the_sub_field('content'); ?>
		    		</div>
		  		</li>
		  	<?php endwhile; ?>
		</ul>
	<?php } ?>
</div>