<?php

/**
 * This is the template that renders the accordion block.
 *
 * @param   array $block The block settings and attributes.
 * @param   bool $is_preview True during AJAX preview.
 */

// create id attribute for specific styling
$id = 'reveal-more-grid-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>

<div class="wp-block-reveal-more-grid <?php echo $align_class; ?>" id="<?php echo $id; ?>">
	<?php if (have_rows('items')) { ?>
		<div class="grid-x grid-margin-x grid-margin-y small-up-1 medium-up-2">
			<?php $item_counter = 1;
			while (have_rows('items')): the_row(); ?>
		  		<a href="#" class="cell card" data-open="item-reveal-<?php echo $item_counter; ?>">
	  				<div class="card-section">
			    		<h2 class="h5"><?php the_sub_field('title'); ?></h2>
			    		<?php if (get_sub_field('content') != "") { ?>
			    		<p class="excerpt">
			    			<?php echo substr(strip_tags(get_sub_field('content')), 0, 150); ?>...<span>Read More</span>
			    		</p>
			    		<?php } ?>
			    	</div>
		  		</a>
				
				<?php if (get_sub_field('content') != "") { ?>
		  		<div class="reveal" id="item-reveal-<?php echo $item_counter; ?>" data-reveal>
					<h2 class="h5"><?php the_sub_field('title'); ?></h2>
					<?php the_sub_field('content'); ?>
					<button class="close-button" data-close aria-label="Close modal" type="button">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?php } ?>

				<?php $item_counter++; ?>
		  	<?php endwhile; ?>
		</div>
	<?php } ?>
</div>