<?php

/**
 * This is the template that renders the testimonial block.
 *
 * @param   array $block The block settings and attributes.
 * @param   bool $is_preview True during AJAX preview.
 */

// create id attribute for specific styling
$id = 'testimonial-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>

<div class="wp-block-testimonial <?php echo $align_class; ?>" id="<?php echo $id; ?>">
	<?php the_field('content'); ?>

	<?php if (get_field('citation') != "") { ?>	
		<cite class="testimonial-source"><?php the_field('citation'); ?></cite>
	<?php } ?>
</div>