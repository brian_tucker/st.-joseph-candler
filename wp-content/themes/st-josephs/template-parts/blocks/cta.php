<?php

/**
 * This is the template that renders the CTA block.
 *
 * @param   array $block The block settings and attributes.
 * @param   bool $is_preview True during AJAX preview.
 */

// create id attribute for specific styling
$id = 'cta-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

$image = get_field('background_image');
$image_class = 'no-image';
if (!empty($image)) {
	$image_class = 'has-image';
} ?>

<div class="wp-block-cta <?php echo $align_class; ?> <?php echo $image_class; ?>" id="<?php echo $id; ?>" data-interchange="[<?php echo $image['sizes']['medium']; ?>, small], [<?php echo $image['sizes']['large']; ?>, medium], [<?php echo $image['sizes']['xlarge']; ?>, large]">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x align-center">
			<div class="cell medium-11 large-10">
				<div class="cta-content">
					<?php the_field('content'); ?>
					<?php $button = get_field('button'); 
					if ( $button ): ?>
						<a href="<?php echo $button['url']; ?>" class="button secondary" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>