<?php

/**
 * This is the template that renders the accordion block.
 *
 * @param   array $block The block settings and attributes.
 * @param   bool $is_preview True during AJAX preview.
 */

// create id attribute for specific styling
$id = 'resource-links-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>

<div class="wp-block-resource-links <?php echo $align_class; ?>" id="<?php echo $id; ?>">
	<?php if (get_field('heading') != "") { ?>
		<h3 class="text-center block-heading"><?php the_field('heading'); ?></h3>
	<?php } ?>
	<?php if (have_rows('resource_links')) { ?>
		<div class="resource-link-grid grid-x grid-margin-x grid-margin-y grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-3">
			<?php while (have_rows('resource_links')): the_row(); ?>
				<?php $link = get_sub_field('link');
				if ($link) { ?>
			  		<a class="cell resource-link grid-y align-middle align-center" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
			  			<div class="cell shrink link-contents grid-x grid-padding-x align-middle">
			  				<div class="cell small-2 medium-12 text-left medium-text-center">
				    			<?php if (get_sub_field('icon') != "other") { ?>
				    				<i class="<?php the_sub_field('icon'); ?>"></i>
				    			<?php } else { ?>
				    				<?php the_sub_field('other_icon_markup'); ?>
				    			<?php } ?>
				    		</div>
				    		<div class="cell small-10 medium-12 text-left medium-text-center">
			    				<?php echo $link['title']; ?>
			    			</div>
			    		</div>
			    	</a>
			  	<?php } ?>
		  	<?php endwhile; ?>
		</div>
	<?php } ?>
</div>