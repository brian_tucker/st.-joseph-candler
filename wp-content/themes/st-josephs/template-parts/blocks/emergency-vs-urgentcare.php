<?php

/**
 * This is the template that renders the CTA block.
 *
 * @param   array $block The block settings and attributes.
 * @param   bool $is_preview True during AJAX preview.
 */

// create id attribute for specific styling
$id = 'emergency-vs-uc-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>

<div class="wp-block-emergency-vs-uc <?php echo $align_class; ?>" id="<?php echo $id; ?>">
	<div class="grid-x">
		<div class="cell small-12 large-6">
			<div class="comparison-container emergency-container text-left large-text-right">
				<div class="heading-container">
					<h2 class="comparison-heading">Emergency</h2>
				</div>
				<ul class="symptom-list" id="emergency-symptoms">
					<li class="symptom-item" data-count="1"><div>Chest pain or squeezing sensation in the chest</div></li>
					<li class="symptom-item" data-count="2"><div>Seizure or loss of consciousness</div></li>
					<li class="symptom-item" data-count="3"><div>Severe abdominal pain</div></li>
					<li class="symptom-item" data-count="4"><div>Sudden paralysis or slurred speech</div></li>
					<li class="symptom-item" data-count="5"><div>Uncontrolled bleeding</div></li>
					<li class="symptom-item" data-count="6"><div>Difficulty breathing</div></li>
					<li class="symptom-item" data-count="7"><div>Loss of vision</div></li>
				</ul>
			</div>	
		</div>
		<div class="cell small-12 large-6">
			<div class="comparison-container urgentcare-container">
				<div class="heading-container">
					<span class="vs">vs</span>
					<h2 class="comparison-heading">Urgent Care</h2>
				</div>
				<ul class="symptom-list" id="urgentcare-symptoms">
					<li class="symptom-item" data-count="1"><div>Ear or Eye infection</div></li>
					<li class="symptom-item" data-count="2"><div>Fever</div></li>
					<li class="symptom-item" data-count="3"><div>Cuts that may need stitches</div></li>
					<li class="symptom-item" data-count="4"><div>Possible broken bones or simple fractures</div></li>
					<li class="symptom-item" data-count="5"><div>Severe sore throat</div></li>
					<li class="symptom-item" data-count="6"><div>Sprains and strains</div></li>
					<li class="symptom-item" data-count="7"><div>Vomiting and diarrhea</div></li>
				</ul>
			</div>	
		</div>
	</div>

	<script>
		function resizeSymptomItems() {
			var emergencySymptom, count, emergencyHeight, urgentcareSymptom, urgentcareHeight, newHeight;
			$('#emergency-symptoms').find('.symptom-item').each(function() {
				emergencySymptom = $(this);
				count = emergencySymptom.data('count');
				emergencyHeight = emergencySymptom.height();
				urgentcareSymptom = $('#urgentcare-symptoms .symptom-item[data-count="' + count + '"]');
				urgentcareHeight = urgentcareSymptom.height();
				newHeight = Math.max(emergencyHeight, urgentcareHeight);
				emergencySymptom.find('div').height(newHeight);
				urgentcareSymptom.find('div').height(newHeight);
			});
		}
		$(document).ready(function() {
			$(window).on('load resize orientationChange', function() {
				if (Foundation.MediaQuery.atLeast('large')) {
					resizeSymptomItems();
				} else {
					$('.symptom-item').each(function() {
						$(this).find('div').height('auto');
					});
				}
			});
		});
	</script>
</div>