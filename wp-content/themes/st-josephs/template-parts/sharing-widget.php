<div class="grid-x grid-margin-x align-justify align-middle sharing-block">
	<div class="cell shrink">
		<p>Share This Post</p>
	</div>
	<div class="cell shrink">
		<div class="button-group small darkgray social-sharing">
			<a class="share button facebook" href="#"><i class="fab fa-facebook-f"></i></a>
			<!-- <a class="share button instagram" href="#"><i class="fab fa-instagram"></i></a> -->
			<a class="share button twitter" href="#"><i class="fab fa-twitter"></i></a>
			<a class="share button linkedin"><i class="fab fa-linkedin-in"></i></a>
			<?php 
				$emailSubject = "Great Article from The Aesthetic Medicine and Anti-Aging Clinics of LA";
				$emailBody = rawurlencode("Check out this article on their website.") . '%0D%0A%0D%0A' . rawurlencode(get_the_title()) . '%0D%0A' . rawurlencode(get_the_permalink());
				$emailLink = "mailto:?subject=" . rawurlencode($emailSubject) . "&body=" . $emailBody;

			 ?>
			<a class="share button email" href="<?php echo $emailLink; ?>" target="_blank"><i class="far fa-envelope"></i></a>
		</div>
	</div>
	<div class="cell small-12">
		<hr>
	</div>
</div>

<script>
	/* ====================
	 * Facebook Initialization
	 * ==================== */
  	window.fbAsyncInit = function() {
    	FB.init({
	      appId            : '399462457616539',
	      autoLogAppEvents : true,
	      xfbml            : true,
	      version          : 'v3.0'
    	});
  	};

  	(function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "https://connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
   	}(document, 'script', 'facebook-jssdk'));


	$('.share.facebook').click(function(event) {
		event.preventDefault();
		var href = window.location.href;
		href = href.replace(":8888", ".com");
		console.log(href);
	  	FB.ui({
	  		display: 'popup',
	    	method: 'share',
	    	href: encodeURI(href),
	  	}, function(response){});
	});

	$('.share.twitter').click(function(event) {
		event.preventDefault();
		var pageTitle = '<?php echo get_the_title(); ?>';
		var text = encodeURI(pageTitle.trim());
		var url = encodeURI(window.location.href);
		var tweetShareLink = "https://twitter.com/intent/tweet?url=" + url + "&text=" + text + "&via=AestheticMedLA";
	  
	  	window.open(tweetShareLink,'pagename','resizable,height=420,width=550');
	});

	$('.share.linkedin').click(function(event) {
		event.preventDefault();
		var pageTitle = '<?php echo get_the_title(); ?>';
		var text = encodeURI(pageTitle.trim());
		var url = encodeURI(window.location.href);
		var linkedinShareLink = "https://www.linkedin.com/shareArticle?url=" + url + "&mini=true&title=" + text + "&source=AestheticMedLA";
	  
	  	window.open(linkedinShareLink,'pagename','resizable,height=420,width=550');
	});
</script>


