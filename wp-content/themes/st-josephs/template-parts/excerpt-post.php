<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>
	<div class="grid-x">
		<div class="featured-thumbnail">
			<?php the_post_thumbnail('thumbnail'); ?>
			<?php foundationpress_entry_meta(); ?>
		</div>
		<div class="info-container">
			<header>
				<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
			</header>
			<?php if (0) { ?>
			<div class="entry-content show-for-medium">
				<?php the_excerpt(); ?>
			</div>
			<?php } ?>
			<footer>
				<a href="<?php the_permalink(); ?>" class="button secondary">Read Article</a>
			</footer>	
		</div>
	</div>
</article>
<hr>
