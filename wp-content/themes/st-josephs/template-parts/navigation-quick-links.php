<?php if (have_rows('navigation_buttons', 'option')) { ?>
	<div class="button-group quick-links expanded">
		<?php while (have_rows('navigation_buttons', 'option')): the_row(); ?>
			<?php $link = get_sub_field('button_link');
			if ($link) { ?>
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="button tiny <?php the_sub_field('button_color'); ?> <?php if (get_sub_field('hide_on_landing_page')) echo 'hide-for-landing-page'; ?>"><?php the_sub_field('button_icon'); ?> <?php echo $link['title']; ?></a>
			<?php } ?>
		<?php endwhile; ?>
	</div>
<?php } ?>