<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.

if (has_post_thumbnail( $post->ID ) ) { ?>
	<header class="featured-hero" role="banner" data-interchange="[<?php the_post_thumbnail_url( 'medium' ); ?>, small], [<?php the_post_thumbnail_url( 'medium' ); ?>, medium], [<?php the_post_thumbnail_url( 'large' ); ?>, large], [<?php the_post_thumbnail_url( 'full' ); ?>, xlarge]">
		<div class="cell">
			<div class="main-container">
				<div class="grid-x grid-padding-x align-center">
					<div class="cell small-12 medium-11 large-10">
						<h1 class="entry-title"><?php the_title(); ?></h1>		
					</div>
				</div>
			</div>
		</div>

		<?php //get_template_part('template-parts/watermark'); ?>
	</header>
<?php } else { ?>
	<header class="simple-title">
		<div class="main-container">
			<div class="grid-x grid-padding-x align-center">
				<div class="cell medium-11 large-10">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</header>
<?php } ?>
