<?php
/**
 * Template part for off canvas menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<nav class="mobile-off-canvas-menu off-canvas position-right" id="<?php foundationpress_mobile_menu_id(); ?>" data-off-canvas data-auto-focus="false" data-transition="overlap" data-content-scroll="false" role="navigation">
	<div class="grid-x" id="off-canvas-button-group">
		<div class="cell small-12 large-shrink">
			<?php get_template_part('template-parts/navigation-quick-links'); ?>
		</div>
	</div>

	<?php foundationpress_mobile_nav(); ?>

	<script>
		$(document).ready(function() {
			$('#menu-main-menu').on('up.zf.accordionMenu', function() {
			  $(this).find('li').each(function() {
			  	$(this).removeClass('expanded');
			  });
			});
			$('#menu-main-menu').on('down.zf.accordionMenu', function() {
			  $(this).find('button[aria-expanded="true"]').parent('li').addClass('expanded');
			});
		});
	</script>
</nav>

<?php get_template_part( 'template-parts/help-information-pane' ); ?>

<div class="off-canvas-content" data-off-canvas-content>
