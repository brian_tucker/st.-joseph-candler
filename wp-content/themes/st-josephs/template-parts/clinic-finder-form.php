<?php  ?>
<form action="<?php echo get_post_type_archive_link( 'location' ); ?>" method="POST" id="clinic-finder-form" data-abide novalidate>
	<div data-abide-error class="alert callout" style="display: none;">
    	<p><i class="fi-alert"></i> There are some errors in your form.</p>
  	</div>
	<div class="grid-x grid-padding-x align-bottom align-center">
		<div class="cell small-6 large-4">
			<label>Search by ZIP Code
				<div id="zip-container">
					<input type="text" id="zip" name="zip" placeholder="Enter ZIP" pattern="zip_code" value="<?php echo $_POST['zip']; ?>">
					<button type="submit" class="button hide" id="zip-submit">Go</button>
				</div>					
			</label>
		</div>
		<div class="cell small-6 large-4" id="geolocation-container">
			<button class="button secondary expanded" id="geolocation-button"><i class="far fa-location"></i> Use My Location</button>
			<input type="hidden" name="latitude" id="latitude" value="<?php echo $_POST['latitude']; ?>">
			<input type="hidden" name="longitude" id="longitude" value="<?php echo $_POST['longitude']; ?>">
		</div>
		<div class="cell small-12 large-4">
			<label>Filter by City
				<select name="city" id="city">
					<option value="">Select City</option>
					<?php 
					$args = array(
						'post_type' => 'location',
						'posts_per_page' => -1,

					);
					
					$cities = new WP_Query($args);
					while($cities->have_posts()) { $cities->the_post();
						$city_meta = get_field('address');
						echo '<option value="' . $city_meta['city'] . '"';
						if ($_POST['city'] == $city_meta['city'] ) {
							echo ' selected="selected"';
						}
						echo '>' . $city_meta['city'] . '</option>';
					} 
					wp_reset_postdata();
					wp_reset_query(); ?>
				</select>
			</label>
		</div>
	</div>
	<span class="form-error" data-form-error-for="zip">
        Must be a valid ZIP code
    </span>
    <span class="form-error" id="geolocation-result"></span>
    <input type="submit" class="hide" value="Submit" id="clinic-finder-submit">

    <?php if (($_POST['zip'] != "") || ($_POST['latitude'] != "") || ($_POST['longitude'] != "") || ($_POST['city'] != "")) { ?>
    	<p class="text-center"><a href="#" id="clear-filter" class="alert"><i class="far fa-times"></i> Clear filter</a></p>
    <?php } ?>
</form>

<script>
	$(document).ready(function() {
		$('#geolocation-button').click(function(event) {
			event.preventDefault();
			getLocation();
		});

		$('#zip-submit').click(function(event) {
			event.preventDefault();
			$('#city').find('option:selected').removeAttr('selected');
			$('#clinic-finder-submit').click();
		});

		$("#zip").on("invalid.zf.abide", function(ev,el) {
		  	// take action if zip field is still invalid
		  	$('#zip-submit').addClass('hide');
		});
		$("#zip").on("valid.zf.abide", function(ev,el) {
			if ($('#zip').val() != "") {
		  		// take action if zip field is valid and not empty
				$('#zip-submit').removeClass('hide');
			}
		});

		var citySelect = document.getElementById("city");
		function onCityChange() {
			if ( citySelect.options[citySelect.selectedIndex].value != "" ) {
				$('#zip').val('');
				$('#clinic-finder-submit').click();
			}
		}
		citySelect.onchange = onCityChange;

		$('#clear-filter').click(function(event) {
			event.preventDefault();
			$('#zip').val('');
			$('#latitude').val('');
			$('#longitude').val('');
			$('#city').find('option:selected').removeAttr('selected');
			$('#clinic-finder-submit').click();
		});
	});
</script>