<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php get_template_part( 'template-parts/featured-image' ); ?>

		<div class="main-container">
			<div class="main-grid">
				<main class="main-content">
					<div class="entry-content">
						<div class="grid-x grid-margin-x grid-margin-y">
							<div class="cell medium-6">
								<p class="address lead"><?php echo get_address_string($post->ID); ?></p>
								<p><a class="phone lead" href="tel:<?php echo stripPhoneForLink(get_field('phone')); ?>"><?php the_field('phone'); ?></a></p>	
							</div>
							<div class="cell medium-6 medium-text-right">
								<?php 
									$clockwisemd_id = get_field('clockwisemd_id'); 
									$virtual_visit = get_field('virtual_visit_clockwisemd_id'); 
									$hidden = get_field('hide_virtual_visit');
								?>
								

								<?php if ($clockwisemd_id != "") { ?>
									<div class="button-group small expanded">
										<?php if($hidden): ?>

										<?php else: ?>
											<?php if ($virtual_visit != "") { ?>
												<a href="https://www.clockwisemd.com/hospitals/<?php echo $virtual_visit; ?>/visits/new" class="button secondary" target="_blank">Virtual Visit</a>
											<?php } else { ?>
												<a href="https://www.clockwisemd.com/hospitals/<?php echo $clockwisemd_id; ?>/visits/new" class="button secondary" target="_blank">Virtual Visit</a>
											<?php } ?>
										<?php endif; ?>
										<a href="https://www.clockwisemd.com/hospitals/<?php echo $clockwisemd_id; ?>/visits/new" class="button" target="_blank">Check-In</a>
									</div>
									
								<?php } ?>
								<div class="ride-services grid-x grid-margin-x">
									<div class="cell shrink"><p>Need a ride?</p></div>
									<div class="cell shrink">
										<?php $uber_url = 'https://m.uber.com/ul/?action=setPickup&pickup=my_location'; 
										$uber_url .= '&' . urlencode('dropoff[formatted_address]') . '=' . urlencode(str_replace("<br>", " ", get_address_string($post->ID)));
										$uber_url .= '&' . urlencode('dropoff[latitude]') . '=' . $location['lat'];
										$uber_url .= '&' . urlencode('dropoff[longitude]') . '=' . $location['lng']; ?>
										<a href="<?php echo $uber_url; ?>" class="uber" title="Get a ride with Uber" target="_blank">
											<i class="fab fa-uber"></i><span class="show-for-sr">Get a ride with Uber</span>
										</a>
										<?php $lyft_url = 'https://lyft.com/ride?id=lyft&partner=NuWwQpN9mi2J';
										// $lyft_url .= urlencode('&dropoff[formatted_address]=' . str_replace("<br>", " ", get_address_string($post->ID)));
										$lyft_url .= '&' . urlencode('pickup[latitude]') . '=' . $_POST['latitude'];
										$lyft_url .= '&' . urlencode('pickup[longitude]') . '=' . $_POST['longitude'];
										$lyft_url .= '&' . urlencode('destination[latitude]') . '=' . $location['lat'];
										$lyft_url .= '&' . urlencode('destination[longitude]') . '=' . $location['lng']; 

										$full_clinic_name = get_field('full_clinic_name');
										if ($full_clinic_name == "") $full_clinic_name = 'Lake After Hours - ' . get_the_title(); ?>
										<a href="<?php echo $lyft_url; ?>" class="lyft" title="Get a ride with Lyft" target="_blank" data-post-id="<?php echo $post->ID; ?>">
											<i class="fab fa-lyft"></i><span class="show-for-sr">Get a ride with Lyft</span>
										</a>
										<div class="reveal" id="reveal-<?php echo $post->ID; ?>" data-reveal>
											<p>We cannot connect to Lyft without your current location. Please proceed to the Lyft app with the link below and search for '<?php echo $full_clinic_name; ?>' as your destination.</p>
										</div>
										<script>
											$(document).ready(function() {
												<?php if ((!isset($_POST['latitude'])) || ($_POST['latitude'] == '')) { ?>
													$('a.lyft').click(function(event) {
														event.preventDefault();
														var link_url = $(this).attr('href');
														var post_id = $(this).data('post-id');
														console.log('reveal-'+post_id);
														$('#reveal-'+post_id).foundation('open');
													});
												<?php } ?>
											})
										</script>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="grid-x grid-margin-x grid-padding-y">
							<div class="cell large-7">
								<?php the_post_thumbnail('medium'); ?>
							</div>
							<div class="cell large-5">
								<?php $logo = get_field('logo');
								if (!empty($logo)) { ?>
									<img class="location-logo" src="<?php echo $logo['sizes']['medium']; ?>" alt="logo for location">
								<?php } ?>
								<p class="hours">Office Hours:<br><?php the_field('hours'); ?></p>
								
								<?php $location = get_field('location');
								if( $location ): ?>
									<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key; ?>"></script>
									<div id="location-map" class="google-map acf-map" data-map-type-control="false">
										<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
									</div>
									<?php $directions_url = 'https://www.google.com/maps/dir/?api=1&origin=&destination=' . urlencode(str_replace("<br>", " ", get_address_string($post->ID)));
									if (get_field('google_place_id') != "") {
										$directions_url .= '&destination_place_id=' . get_field('google_place_id');
									} ?>
									<p class="text-right"><small><a href="<?php echo $directions_url; ?>" class="alert directions-link" target="_blank">Get Directions</a></small></p>
								<?php endif; ?>
							</div>
						</div>

						<?php the_content(); ?>
					</div>
					<footer>
						<?php
							wp_link_pages(
								array(
									'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
									'after'  => '</p></nav>',
								)
							);
						?>
						<?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
					</footer>
				</main>
			</div>
	</article>

	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "MedicalClinic",
		"address": {
			"@type": "PostalAddress",
			"addressLocality": "<?php the_field('address_city'); ?>",
			"addressRegion": "<?php the_field('address_state'); ?>",
			"postalCode": "<?php the_field('address_zip'); ?>",
			"streetAddress": "<?php the_field('address_street'); if (get_field('address_street_line_2') != "") echo ', ' . get_field('address_street_line_2'); ?>"
		},
		"name": "<?php echo $full_clinic_name; ?>",
		"openingHours": "<?php echo strip_tags(get_field('hours')); ?>",
		"telephone": "<?php the_field('phone'); ?>",
		"url": "<?php the_permalink(); ?>",
		"image": "<?php echo get_the_post_thumbnail_url( null, 'thumbnail' ); ?>",
		<?php if (!empty($logo)) { ?>"logo": "<?php echo $logo['sizes']['medium']; ?>",<?php } ?>
		"isAcceptingNewPatients": true,
	}
	</script>
<?php endwhile; ?>

<?php get_footer();
