<?php

function custom_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'custom_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

	$style_formats = array(
		// Each array child is a format with it's own settings
		
		// array(
		// 	'title' => 'Subheader',
		// 	'selector' => 'h1,h2,h3,h4,h5,h6',
		// 	'classes' => 'subheader',
		// 	'wrapper' => false,
		// ),
		array(
			'title' => 'Lead In Paragraph',
			'selector' => 'p',
			'classes' => 'lead',
			'wrapper' => false,
		),
		array(
			'title' => 'Button',
			'selector' => 'a',
			'classes' => 'button',
			'wrapper' => false,
		),
		array(
			'title' => 'Secondary Button',
			'selector' => 'a',
			'classes' => 'button secondary',
			'wrapper' => false,
		),
		array(
			'title' => 'Hollow Button',
			'selector' => 'a',
			'classes' => 'button hollow',
			'wrapper' => false,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

// function my_theme_add_editor_styles() {
//     add_editor_style( 'custom-editor-style.css' );
// }
// add_action( 'init', 'my_theme_add_editor_styles' );
