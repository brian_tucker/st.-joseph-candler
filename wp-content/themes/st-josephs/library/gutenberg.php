<?php

if ( ! function_exists( 'foundationpress_gutenberg_support' ) ) :
	function foundationpress_gutenberg_support() {

    // Add foundation color palette to the editor
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'Primary Color', 'foundationpress' ),
            'slug' => 'primary',
            'color' => '#205A90',
        ),
        array(
            'name' => __( 'Secondary Color', 'foundationpress' ),
            'slug' => 'secondary',
            'color' => '#BA3744',
        ),
    ) );

    add_theme_support( 'disable-custom-colors' );

	}

	add_action( 'after_setup_theme', 'foundationpress_gutenberg_support' );

    /**
     * Templates and Page IDs without editor
     *
     */
    function ea_disable_editor( $id = false ) {

        $excluded_templates = array(
            'page-templates/front.php',
        );

        $excluded_ids = array(
            // get_option( 'page_on_front' )
        );

        $excluded_post_types = array(
            'location',
        );

        if( empty( $id ) )
            return false;

        $id = intval( $id );
        $template = get_page_template_slug( $id );
        $post_type = get_post_type( $id );

        return (in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates ) || in_array( $post_type, $excluded_post_types ));
    }

    /**
     * Disable Gutenberg by template
     *
     */
    function ea_disable_gutenberg( $can_edit, $post_type ) {

        if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
            return $can_edit;

        if( ea_disable_editor( $_GET['post'] ) )
            $can_edit = false;

        return $can_edit;

    }
    add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
    add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );
endif;


function myguten_enqueue() {
    wp_enqueue_script(
        'myguten-script',
        get_stylesheet_directory_uri() . '/dist/assets/js/core-block-filters.js',
        array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post', 'wp-element' ),
        //filemtime( get_stylesheet_directory_uri() . '/dist/assets/js/core-block-filters.js' )
    );
}
add_action( 'enqueue_block_editor_assets', 'myguten_enqueue' );

// Update CSS within in Admin
function admin_style() {
  wp_enqueue_style('editor-styles', get_stylesheet_directory_uri().'/dist/assets/css/editor.css');
}
add_action('admin_enqueue_scripts', 'admin_style');