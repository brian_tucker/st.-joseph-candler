<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<header class="featured-hero" role="banner" data-interchange="[<?php echo get_the_post_thumbnail_url(get_option('page_for_posts', true), 'medium' ); ?>, small], [<?php echo get_the_post_thumbnail_url(get_option('page_for_posts', true), 'medium' ); ?>, medium], [<?php echo get_the_post_thumbnail_url(get_option('page_for_posts', true), 'large' ); ?>, large], [<?php echo get_the_post_thumbnail_url(get_option('page_for_posts', true), 'full' ); ?>, xlarge]">
	<div class="cell">
		<div class="grid-container">
			<div class="grid-x grid-margin-x grid-padding-x align-center">
				<div class="cell small-12 medium-11 large-10">
					<h1 class="entry-title"><?php echo get_the_title(get_option('page_for_posts', true)); ?></h1>		
				</div>
			</div>
		</div>
	</div>

	<?php get_template_part('template-parts/clock'); ?>
</header>

<?php get_template_part('template-parts/news-filter'); ?>

<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
if ($paged == 1) {
	// WP_Query arguments
	$args = array(
		'post_type'              => array( 'post' ),
		'post_status'            => array( 'publish' ),
		'posts_per_page'         => '1',
		'category_name'			 => 'featured',
	);

	// The Query
	$featured_query = new WP_Query( $args );

	// The Loop
	if ( $featured_query->have_posts() ) {
		while ( $featured_query->have_posts() ) {
			$featured_query->the_post(); ?>
			<div class="featured-article">
				<div class="grid-x">
					<div class="cell large-3 image-container" style="background-image: url('<?php echo get_the_post_thumbnail_url( null, 'medium' ); ?>')">
						<?php echo '<time class="updated entry-date" datetime="' . get_the_time( 'c' ) . '">' . sprintf( __( '%1$s', 'foundationpress' ), get_the_date('m/d/y'), get_the_time() ) . '</time>'; ?>
						<div class="entry-featured-image hide-for-medium">
							<?php the_post_thumbnail('medium'); ?>
						</div>
					</div>
					<div class="cell large-9 info-container">
						<div class="entry-info">
							<p class="featured-label">Featured Article</p>
							<h2 class="h1 entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<a href="<?php the_permalink(); ?>" class="button white">Read Article</a>
						</div>
					</div>
				</div>
			</div>
		<?php }
	} else {
		// no posts found
	}

	// Restore original Post Data
	wp_reset_postdata();
} ?>

<div class="main-container">
	<div class="main-grid">
		<main class="main-content">
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/excerpt', get_post_type() ); ?>
				<?php endwhile; ?>

			<?php else : ?>
				<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif; // End have_posts() check. ?>

			<?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php
			if ( function_exists( 'foundationpress_pagination' ) ) :
				foundationpress_pagination();
			elseif ( is_paged() ) :
			?>
				<nav id="post-nav">
					<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
					<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
				</nav>
			<?php endif; ?>

		</main>
	</div>
</div>

<?php get_footer();
