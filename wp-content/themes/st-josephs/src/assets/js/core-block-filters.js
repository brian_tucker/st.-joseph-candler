( function() {
	wp.domReady( function() {
		// wp.blocks.registerBlockStyle( 'gravityforms/form', {
		//     name: 'male-bg',
		//     label: 'Male in Background'
		// } );
		// wp.blocks.registerBlockStyle( 'gravityforms/form', {
		//     name: 'female-bg',
		//     label: 'Female in Background'
		// } );

		wp.blocks.unregisterBlockStyle( 'core/quote', 'large' );
		wp.blocks.unregisterBlockStyle( 'core/button', 'squared' );
		wp.blocks.unregisterBlockStyle( 'core/button', 'outlined' );
	} );
})();