// function zipCodeValidator(
//   $el,      /* jQuery element to validate */
//   required, /* is the element required according to the `[required]` attribute */
//   parent    /* parent of the jQuery element `$el` */
// ) {
//   if (!required) return true;
//   var submittedValue = $el.val();

//   return (parseInt(to) > parseInt(from));
// };

// // Set default options
// Foundation.Abide.defaults.validators['zip_code'] = zipCodeValidator;

Foundation.Abide.defaults.patterns['zip_code'] = /^[0-9]{5}(?:-[0-9]{4})?$/;