(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

// create info window outside of each - then tell that singular infowindow to swap content based on click
var infowindow = null;

window.new_map = function( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom				: 16,
		center				: new google.maps.LatLng(0, 0),
		mapTypeId			: google.maps.MapTypeId.ROADMAP,
		mapTypeControl		: $el.data('map-type-control'),
		fullscreenControl 	: false,
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		if ($(this).hasClass('current-location')) {
			add_current_location_marker( $(this), map );
		} else {
			add_marker( $(this), map );
		}
	});
	
	
	// center map
	center_map( map );

	// create info window outside of each - then tell that singular infowindow to swap content based on click
	infowindow = new google.maps.InfoWindow({
		content     : '',
	});
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

window.add_marker = function( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	var icon = {
	    url: themeDirectory + '/dist/assets/images/map-marker-plus-solid.svg',
	    scaledSize: new google.maps.Size(23, 30), // scaled size
	    origin: new google.maps.Point(0, 0), // origin
	    anchor: new google.maps.Point(12, 30) // anchor
	};

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		icon        : icon,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent($marker.html());
			infowindow.open( map, marker );

		});

		// close info window when map is clicked
        google.maps.event.addListener(map, 'click', function(event) {
	        if (infowindow) {
	            infowindow.close(); 
	        }
	    });
	}

}

/*
*  add_current_location_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

window.add_current_location_marker = function( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	var iconPath = $marker.attr('data-icon-path');
	if (iconPath = 'circle') {
		iconPath = google.maps.SymbolPath.CIRCLE;
	}
	var fillColor = $marker.attr('data-fill-color');
	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		icon: {
			path: iconPath,
			scale: 8,
			fillColor: fillColor,
			fillOpacity: 1.0,
			strokeColor: '#ffffff',
			strokeWeight: 3
		},
		animation: google.maps.Animation.DROP,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent($marker.html());
			infowindow.open( map, marker );

		});

		// close info window when map is clicked
        google.maps.event.addListener(map, 'click', function(event) {
	        if (infowindow) {
	            infowindow.close(); 
	        }
	    });
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

window.center_map = function( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){
	$('.acf-map').each(function(){
		// create map
		map = new_map( $(this) );
	});
});

})(jQuery);


window.calculateDistance = function($el, origin, destination) {
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
		origins: [origin],
		destinations: [destination],
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.IMPERIAL,
		avoidHighways: false,
		avoidTolls: false
    }, function(response, status) {
	    if (status != google.maps.DistanceMatrixStatus.OK) {
	      	$el.attr('status', err);
	    } else {
	    	// console.log(response);
	      	var origin = response.originAddresses[0];
	      	var destination = response.destinationAddresses[0];
	      	if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
	        	$el.attr('status', "Better get on a plane. There are no roads between " + origin + " and " + destination);
	      	} else {
	        	var distance = response.rows[0].elements[0].distance;
	        	var distance_value = distance.value;
	        	var distance_text = distance.text;
	        	// console.log(distance);
	        	$el.attr('data-distance', distance_value);
	        	$el.find('.distance-value').text(distance_text);
	        	$el.find('.distance').removeClass('hide');
	        	sortLocationsByDistance();
	      	}
	    }
	});
}