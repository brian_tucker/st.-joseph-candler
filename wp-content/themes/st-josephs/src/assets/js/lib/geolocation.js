var form = document.getElementById("clinic-finder");
var submitButton = document.getElementById("clinic-finder-submit");
var latitudeField = document.getElementById("latitude");
var longitudeField = document.getElementById("longitude");
var zipField = document.getElementById("zip");
var cityField = document.getElementById("city");
var geolocationContainer = document.getElementById("geolocation-container");
var geolocationButton = document.getElementById("geolocation-button");
var geolocationResult = document.getElementById("geolocation-result");

window.getLocation = function() {
  $(geolocationButton).find('i').removeClass('fa-location').addClass('fa-spinner fa-spin');
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition, showError);
  } else {
    console.log('no geolocation value');
    $(geolocationButton).find('i').removeClass('fa-spinner fa-spin').addClass('fa-exclamation-circle');
    geolocationResult.innerHTML = "Geolocation is not supported by this browser.";
  }
}

window.showPosition = function(position) {
  console.log('Coords: ' + position.coords.latitude + ', ' + + position.coords.longitude);

  latitudeField.value = position.coords.latitude;
  longitudeField.value = position.coords.longitude;
  // console.log('Lat: ' + position.coords.latitude + ' -- Lng: ' + position.coords.longitude);

  $(geolocationButton).find('i').removeClass('fa-spinner fa-spin').addClass('fa-check-circle');
  // $(zipField).attr('value', '');
  // $(cityField).find('option:selected').removeAttr('selected');
  $(submitButton).click();
}

window.showError = function(error) {
  $(geolocationButton).find('i').removeClass('fa-spinner fa-spin').addClass('fa-exclamation-circle');
  console.log('error: ' + error);
  switch(error.code) {
    case error.PERMISSION_DENIED:
      geolocationResult.innerHTML = "You or your browser denied the request for Geolocation.";
      break;
    case error.POSITION_UNAVAILABLE:
      geolocationResult.innerHTML = "Location information is unavailable.";
      break;
    case error.TIMEOUT:
      geolocationResult.innerHTML = "The request to get your location timed out.";
      break;
    case error.UNKNOWN_ERROR:
      geolocationResult.innerHTML = "An unknown error occurred.";
      break;
    default:
      geolocationResult.innerHTML = "An unknown error occurred.";
  }

  $(geolocationResult).addClass('is-visible');
}