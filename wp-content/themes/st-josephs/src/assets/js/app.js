import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

// import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './lib/foundation-explicit-pieces';
import './lib/cookies';
import './lib/custom-validator';
import './lib/geolocation';
import './lib/acf-maps';

$(document).foundation();

$(document).ready(function() {
	$('#help-information-pane').on('opened.zf.offcanvas', function() {
	  	$('#off-canvas-menu').foundation('close');
	  	$('#mobile-menu-toggle').removeClass('toggled');
	});
	$('#off-canvas-menu').on('opened.zf.offcanvas', function() {
	  	$('#help-information-pane').foundation('close');
	});
});