<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<script src="https://kit.fontawesome.com/4af5efc2f5.js" crossorigin="anonymous" data-search-pseudo-elements defer></script>

		<?php wp_head(); ?>
		<script>
			var themeDirectory = '<?php echo get_stylesheet_directory_uri(); ?>';
		</script>
<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJKJ69P');</script>
<!-- End Google Tag Manager -->


	</head>
	<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJKJ69P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>


	<header class="site-header" role="banner">
		<a class="show-on-focus" id="skip-to-main-content" href="#main-content">Skip to Content</a>

		<?php if (is_page_template( 'page-templates/landing-page.php' )) { ?>
			<nav class="lp-navigation" role="navigation">
				<div class="grid-container">
					<div class="grid-x grid-padding-x grid-padding-y align-middle align-justify">
						<div class="cell small-12 large-4">
							<div class="site-desktop-title top-bar-title">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<?php $logo = get_field('logo','options'); ?>
									<img src="<?php echo $logo['sizes']['medium_large']; ?>" alt="<?php echo $logo['alt'] ?>" class="logo">
									
								
								</a>
							</div>
						</div>
						<div class="cell large-7 show-for-large">
							<div class="grid-x align-right" id="top-bar-button-group">
								<div class="cell small-12 large-shrink">
									<?php get_template_part('template-parts/navigation-quick-links'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>
		<?php } else { ?>

			<div class="site-title-bar title-bar mobile-navigation-bar" <?php foundationpress_title_bar_responsive_toggle(); ?> data-hide-for="large">
				<div class="title-bar-left">
					<span class="site-mobile-title title-bar-title">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<?php $logo = get_field('logo','options'); ?>
							<img src="<?php echo $logo['sizes']['medium_large']; ?>" alt="<?php echo $logo['alt'] ?>" class="logo">
						</a>
					</span>
				</div>
				<div class="title-bar-right">
					<div class="grid-x">
						<div class="cell shrink">
							<button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" id="mobile-menu-toggle" type="button" data-toggle="<?php foundationpress_mobile_menu_id(); ?> mobile-menu-toggle" data-toggler="toggled">
								<span></span>
								<span></span>
								<span></span>
							</button>
						</div>
						<div class="cell shrink">
							<button aria-label="<?php _e( 'Information Pane', 'foundationpress' ); ?>" type="button" class="button lightgray" id="information-pane-toggle" data-toggle="help-information-pane"><i class="far fa-question-circle"></i></button>
						</div>
						<?php if (get_field('site_alert_display', 'option')) { ?>
							<div class="cell shrink">
								<button id="return-site-alert" class="hide button alert small squared return-site-alert"><i class="far fa-exclamation-triangle"></i> <span class="show-for-sr">Display Site Alert</span></button>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>

			<nav class="site-navigation top-bar desktop-navigation-bar" role="navigation" id="<?php foundationpress_mobile_menu_id(); ?>" data-animate="hinge-in-from-top spin-out">
				<div class="top-bar-left">
					<div class="site-desktop-title top-bar-title">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<?php $logo = get_field('logo','options'); ?>
							<img src="<?php echo $logo['sizes']['medium_large']; ?>" alt="<?php echo $logo['alt'] ?>" class="logo">
						</a>
					</div>
				</div>
				<div class="top-bar-right large-7">
					<div class="grid-x align-right" id="top-bar-button-group">
						<?php // fix shrink for ie & edge - JHL 1/7/19 ?> 
						<div class="cell small-12 large-8">
						<!-- <div class="cell small-12 large-shrink"> -->
							<?php get_template_part('template-parts/navigation-quick-links'); ?>
						</div>
					</div>

					<?php foundationpress_top_bar_r(); ?>
				</div>
			</nav>
		<?php } ?>
	</header>

	<?php if (!is_page_template( 'page-templates/landing-page.php' )) {
		if (get_field('site_alert_display', 'option')) { ?>
			<script>
				$(document).ready(function() {
					var alert = $('#site-alert');
					$('#close-site-alert').click(function(event) {
						event.preventDefault();
						Foundation.Motion.animateOut(alert, 'slide-out-up');
						setCookie('dismissedSiteAlert', '1', 1);
						$('.return-site-alert').removeClass('hide');
					});

					$('#read-site-alert').click(function(event) {
						setCookie('dismissedSiteAlert', '1', 1);
					});

					$('.return-site-alert').click(function(event) {
						event.preventDefault();
						Foundation.Motion.animateIn(alert, 'slide-in-down');
						setCookie('dismissedSiteAlert', '0', 1);
						$('.return-site-alert').addClass('hide');
					});


					$(window).bind('load', function() {
						if (getCookie("dismissedSiteAlert") != '1') {
							Foundation.Motion.animateIn(alert, 'slide-in-down');
						} else {
							$('.return-site-alert').removeClass('hide');
						}
					});
				});
			</script>

			<div id="site-alert" style="display: none;">
				<div id="alert-label">Alert</div>
				<div class="grid-container">
					<div class="grid-x grid-padding-x grid-padding-y align-middle">
						<div class="cell auto">
							<h4 id="alert-title"><?php the_field('site_alert_title', 'option'); ?></h4>
							<p class="show-for-large">
								<?php the_field('site_alert_description', 'option'); ?>
							</p>
						</div>
						<div class="cell shrink">
							<a href="<?php the_field('site_alert_url', 'option'); ?>" class="button secondary" id="read-site-alert"><i class="far fa-plus hide-for-large"></i> <span class="show-for-large">More Info</span></a>
							<a href="#" class="button midgray" id="close-site-alert"><i class="far fa-times"></i><span class="show-for-sr">Close Site Alert</span></a>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>

		<button aria-label="<?php _e( 'Information Pane', 'foundationpress' ); ?>" type="button" class="button small darkgray show-for-large" id="desktop-information-pane-toggle" data-toggle="help-information-pane"><i class="far fa-question-circle"></i> Help</button>

		<?php if (get_field('site_alert_display', 'option')) { ?>
			<button id="desktop-return-site-alert" class="hide button alert small return-site-alert show-for-large"><i class="far fa-exclamation-triangle"></i> Alert</button>
		<?php } ?>
	<?php } ?>
	<script>
		$(document).ready(function() {
			//$('#mobile-menu').css('display','block');
		});
	</script>
	
	<section id="main-content">